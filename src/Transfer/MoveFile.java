package Transfer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MoveFile {
  
	  public void moveFiles(String src, String dest ) throws IOException {
	      Path result = null;
	      FileTransfer Ft= new FileTransfer();
	     try {
	         result =  Files.move(Paths.get(src), Paths.get(dest));
	      } catch (IOException e) {
	    	  Files.move(Paths.get(src), Paths.get(Ft.ErrorDir));
	         System.out.println("Exception while moving file: " + e.getMessage());
	      }
	      if(result != null) {
	        System.out.println("File moved successfully.");
	      }else{
	         System.out.println("File movement failed.");
	      }  
	   }
}
